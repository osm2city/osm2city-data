from osm2city.textures.texture import Texture

# For some reason the following always results in grey facades.
#facades.append(Texture('de/residential/hotel_18x40m.png',
#    h_size_meters=17.7, h_cuts=[1250], h_can_repeat=True,
#    v_size_meters=40.0, v_cuts=[378, 595, 805, 1018, 1227, 1437, 2818], v_can_repeat=False,
#    v_align_bottom=True, height_min=19.6,
#    requires=[],
#    provides=['shape:urban','shape:residential','age:modern','compat:roof-flat']))

facades.append(Texture('de/industrial/facade_industrial_red_white_24x18m.jpg',
                       23.8, [364, 742, 1086], True,
                       18.5, [295, 565, 842], False,
                       v_align_bottom=True,
                       requires=[],
                       provides=['shape:industrial', 'age:old', 'compat:roof-flat', 'compat:roof-pitched']))

facades.append(Texture('de/residential/DSCF9495_pow2.png',
                       14, [585, 873, 1179, 1480, 2048], True,
                       19.4, [274, 676, 1114, 1542, 2048], False,
                       height_max=13.,
                       v_align_bottom=True,
                       requires=['roof:colour:black'],
                       provides=['shape:residential', 'age:old', 'compat:roof-flat', 'compat:roof-pitched']))

facades.append(Texture('de/residential/LZ_old_bright_bc2.png',
                       17.9, [345, 807, 1023, 1236, 1452, 1686, 2048], True,
                       14.8, [558, 1005, 1446, 2048], False,
                       provides=['shape:residential', 'age:old', 'compat:roof-flat', 'compat:roof-pitched']))

facades.append(Texture('de/commercial/facade_modern_21x42m.jpg',
                       43., [40, 79, 115, 156, 196, 235, 273, 312, 351, 389, 428, 468, 507, 545, 584, 624, 662], True,
                       88., [667, 597, 530, 460, 391, 322, 254, 185, 117, 48, 736, 804, 873, 943, 1012, 1080, 1151, 1218, 1288, 1350], False,
                       v_align_bottom=True, height_min=20.,
                       requires=['roof:colour:black'],
                       provides=['age:modern', 'compat:roof-flat', 'shape:commercial', 'shape:urban']))

facades.append(Texture('de/industrial/facade_industrial_white_26x14m.jpg',
                       25.7, [165, 368, 575, 781, 987, 1191, 1332], True,
                       13.5, [383, 444, 501, 562, 621, 702], False,
                       v_align_bottom = True,
                       requires=[],
                       provides=['shape:industrial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('de/commercial/facade_modern_commercial_35x20m.jpg',
                       34.6, [105, 210, 312, 417, 519, 622, 726, 829, 933, 1039, 1144, 1245, 1350], True,
                       20.4, [177, 331, 489, 651, 796], False,
                       v_align_bottom=True,
                       requires=['roof:colour:black'],
                       provides=['shape:commercial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('de/residential/facade_modern36x36_12.png',
                       36., [], True,
                       36., [158, 234, 312, 388, 465, 542, 619, 697, 773, 870, 1024], False,
                       height_min=20,
                       provides=['shape:urban', 'shape:residential', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('de/residential/facade_modern_residential_26x34m.jpg',
                       26.3, [429, 1723, 2142], True,
                       33.9, [429, 666, 919, 1167, 1415, 1660, 1905, 2145, 2761], False,
                       v_align_bottom = True, height_min=20,
                       provides=['shape:urban', 'shape:residential', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('de/residential/DSCF9503_noroofsec_pow2.png',
                       12.85, [360, 708, 1044, 1392, 2048], True,
                       17.66, [556, 1015, 1474, 2048], False,
                       requires=['roof:colour:black'],
                       provides=['shape:residential', 'age:old', 'compat:roof-flat', 'compat:roof-pitched']))

facades.append(Texture('de/residential/DSCF9710.png',
                       29.9, [142, 278, 437, 590, 756, 890, 1024], True,
                       19.8, [130, 216, 297, 387, 512], False,
                       provides=['shape:residential', 'age:old', 'compat:roof-flat', 'compat:roof-pitched']))

facades.append(Texture('de/residential/DSCF9678_pow2.png',
                       10.4, [97, 152, 210, 299, 355, 411, 512], True,
                       15.5, [132, 211, 310, 512], False,
                       provides=['shape:residential', 'shape:commercial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('de/residential/facade_modern_residential_25x15m.jpg',
                       25.0, [436, 1194, 2121, 2809, 3536], True,
                       14.8, [718, 2096], False,
                       v_align_bottom=True,
                       requires=['roof:colour:black'],
                       provides=['shape:residential', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('de/commercial/facade_modern_commercial_red_gray_20x14m.jpg',
                       20.0, [588, 1169, 1750, 2327, 2911, 3485], True,
                       14.1, [755, 1289, 1823, 2453], False,
                       v_align_bottom=True,
                       requires=['roof:colour:black'],
                       provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('de/commercial/facade_modern_commercial_green_red_27x39m.jpg',
                       27.3, [486, 944, 1398, 1765, 2344], True,
                       38.9, [338, 582, 839, 1087, 1340, 1593, 1856, 2094, 3340], False,
                       v_align_bottom=True,
                       requires=['roof:colour:black'],
                       provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('de/residential/DSCF9726_noroofsec_pow2.png',
                       15.1, [321, 703, 1024], True,
                       9.6, [227, 512], False,
                       provides=['shape:residential', 'age:old', 'compat:roof-flat', 'compat:roof-pitched']))

facades.append(Texture('de/residential/wohnheime_petersburger.png',
                       15.6, [215, 414, 614, 814, 1024], False,
                       15.6, [112, 295, 477, 660, 843, 1024], True,
                       height_min=15.,
                       provides=['shape:urban', 'shape:residential', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('de/commercial/facade_modern_black_46x60m.jpg',
                       45.9, [167, 345, 521, 700, 873, 944], True,
                       60.5, [144, 229, 311, 393, 480, 562, 645, 732, 818, 901, 983, 1067, 1154, 1245], False,
                       v_align_bottom = True, height_min=20.,
                       requires=['roof:colour:black'],
                       provides=['shape:urban', 'age:modern', 'compat:roof-flat']))

facades.append(Texture('de/commercial/facade_modern_commercial_46x170m.jpg',
                       h_size_meters=46, h_cuts=[32, 64, 96, 129, 160, 192, 224, 256], h_can_repeat=True, 
                       v_size_meters=170, v_cuts=[88, 126, 164, 203, 241, 344, 395, 447, 472, 501, 527, 554, 578, 603, 633, 659, 684, 896], v_can_repeat=False, 
                       v_align_bottom=True, height_min=40,
                       requires=['roof:colour:black'],
                       provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))
