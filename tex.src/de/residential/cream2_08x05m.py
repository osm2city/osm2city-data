
from osm2city.textures.texture import Texture

facades.append(Texture('de/residential/cream2_08x05m.png',
    h_size_meters=8.1, h_cuts=[475, 997, 1332, 1359, 1400, 1436, 1471], h_can_repeat=True,
    v_size_meters=5.2, v_cuts=[32, 431, 945], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:old', 'age:modern', 'compat:roof-pitched', 'compat:roof-flat']))
