This is the tex.src/ folder of osm2city.py

These textures will go into osm2city's texture atlas.
No need to distribute tex.src/ to your users.

---

The following textures were taken (with permission) from texturer.com and subsequently modified:

facade_industrial_red_white_24x18m.jpg
buildings_industrial_3098_3821_Small_org.jpg
facade_industrial_white_26x14m.jpg
facade_modern_commercial_35x20m.jpg
facade_modern_black_46x60m.jpg
facade_modern_21x42m.jpg
roof_red_1.png

Delivered-To: thomas.albrecht@
Received: by 10.58.144.197 with SMTP id so5csp64131veb;
        Mon, 12 May 2014 02:38:32 -0700 (PDT)
X-Received: by 10.66.124.163 with SMTP id mj3mr53419830pab.38.1399887511888;
        Mon, 12 May 2014 02:38:31 -0700 (PDT)
Subject: Re: Texturer Contact Form message
From: "Texturer.com" <support@texturer.com>
To: thomas.albrecht@

Hi, Thomas
Feel free to use textures in that way in your project.

Regards, Serg

2014-05-10 14:14 GMT+04:00 <support@texturer.com>:

> Name: Thomas Albrecht
> Email: thomas.albrecht@
> Message: Hi,
>
> I&#39;m developing a city generator &quot;osm2city&quot;
> http://wiki.flightgear.org/Osm2city.py for use with the flight simulator
> &quot;Flightgear&quot;. Both osm2city and Flightgear are open source,
> licensed under the Gnu Public License, GPL. The GPL explicitly allows
> commerical use and selling of the software, provided the source code is
> freely available.
>
> I wonder if I can use some of your textures in my project. I would modify
> them slightly. They would reside in a sub-directory along other textures.
> So, in a sense, this directory could count as a texture pack, which your
> license does not allow?
>
> Best regards,
> Thomas
