
from osm2city.textures.texture import Texture

facades.append(Texture('gb/residential/sandstone4_09x05m.png',
    h_size_meters=9.0, h_cuts=[143, 935, 1671, 2576], h_can_repeat=True,
    v_size_meters=4.9, v_cuts=[724, 1752], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:old', 'age:modern', 'compat:roof-pitched', 'compat:roof-flat']))
