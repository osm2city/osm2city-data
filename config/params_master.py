AREA = 'HOLLAND'

PREFIX = "WS20EHLE"
PATH_TO_OUTPUT = "/home/vanosten/custom_scenery/" + PREFIX
PATH_TO_SCENERY = "/home/vanosten/.fgfs/TerraSync"
PATH_TO_OSM2CITY_DATA = "/home/vanosten/develop_vcs/osm2city-data"

FG_ELEV = '/home/vanosten/bin/flightgear/dnc-managed/install/flightgear/bin/fgelev'

DB_HOST = "localhost"
DB_PORT = 5432
DB_NAME = "holland"
DB_USER = "gisuser"
DB_USER_PASSWORD = "Allegra1"


if AREA == 'HOLLAND':
    # around EHLE 4 tiles: 153, 154, 161, 162
    DB_NAME = "holland"  # covers all of the Netherlands

    FLAG_AFTER_2020_3 = True
    FLAG_STG_LOD_RADIUS = True

    C2P_PROCESS_POWERLINES_MINOR = True  # only considered if C2P_PROCESS_POWERLINES is True
    C2P_PROCESS_OVERHEAD_LINES = True

    C2P_PROCESS_TREES = True

    OWBB_LANDUSE_CACHE = True

    FG_ELEV_CACHE = False
    OWBB_USE_BTG_LANDUSE = True
    FLAG_STG_BUILDING_LIST = True

# Example command
# source /home/vanosten/bin/virtualenvs/o2c310/bin/activate
# /home/vanosten/bin/virtualenvs/o2c310/bin/python3 /home/vanosten/develop_vcs/osm2city/build_tiles.py -f params.py -b *5.25_52.5_5.5_52.625 -o -p 1 -l DEBUG

